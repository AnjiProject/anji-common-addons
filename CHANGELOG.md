# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.3.2] - 2018-01-25

### Changed

- Update minimal `anji-core` version to `v0.8.8`

### Removed

- Remove `docker_compose cartridge`

## [0.3.1] - 2018-01-25

### Added

- Add `docker-compose` cartridge

### Changed

- Update syntax for tasks

### Removed

- Remove pipfile suppor

## [0.3.0] - 2017-12-20

### Changed

- Use pipfile name
- Package renamed with '-'

## [0.2.0] - 2017-12-18

### Fixed

- Compitability to new anjiorm

## [0.1.0] - 2017-12-04

### Changes

- Separate to additional package

[0.3.2]: https://gitlab.com/AnjiProject/anji-common-addons/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/AnjiProject/anji-common-addons/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/AnjiProject/anji-common-addons/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/AnjiProject/anji-common-addons/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/AnjiProject/anji-common-addons/compare/3f5b7517650db83e5e856f1908811f123e71947c...v0.1.0