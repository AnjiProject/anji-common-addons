#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

setup(
    name='anji-common-addons',
    version='0.3.2',
    description="Common addons for AnjiProject",
    long_description=readme,
    author="Bogdan Gladyshev",
    author_email='siredvin.dark@gmail.com',
    url='https://gitlab.com/AnjiProject/anji-common-addons',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "anji-core>=0.8.8"
    ],
    license="MIT license",
    zip_safe=False,
    keywords='chatops errbot slack telegram',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    tests_require=[],
    setup_requires=[],
)
