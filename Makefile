check:
	pylint anji_common_addons
	pycodestyle anji_common_addons
	mypy anji_common_addons --ignore-missing-imports
