=========================
AnjiProject Common Addons
=========================


.. image:: https://img.shields.io/pypi/v/anji-core.svg
        :target: https://pypi.python.org/pypi/anji_common_addons

Common addons for AnjiProject


* Free software: MIT license


Features
--------

* Git addon
* Odoo addon
* Makefile addon
